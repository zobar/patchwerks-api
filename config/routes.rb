Patchwerks::Api::Application.routes.draw do
  resources :census_tracts, only: :show

  resources :counties, only: [:index, :show] do
    resources :census_tracts, only: :index
  end

  resources :states, only: [:index, :show] do
    resources :census_tracts, only: :index
    resources :counties,      only: :index
  end
end
