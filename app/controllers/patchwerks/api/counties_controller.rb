#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
module Patchwerks::Api

  #
  # Controller that manages Counties.
  #
  class CountiesController < ApplicationController
    helper 'patchwerks/api/svg'
    helper_method :county, :counties, :state

    #
    # GET /counties.svg
    # GET /states/36/counties.svg
    #
    def index
      respond_to do |format|
        format.svg {respond_with counties}
      end
    end

    #
    # GET /counties/36029.svg
    #
    def show
      respond_to do |format|
        format.svg {respond_with county}
      end
    end

    protected

      #
      # Fetches the requested County.
      #
      def county
        return @county if instance_variable_defined? :@county
        @county = Patchwerks::Local::County.for_geoid params[:id]
      end

      #
      # If the request contains a +state_id+, fetches all Counties for the State
      # with that +geoid+. If the request does not contain a +state_id+, fetches
      # all Counties.
      #
      def counties
        return @counties if instance_variable_defined? :@counties
        @counties = Patchwerks::Local::County.for_parent_geoid(
            params[:state_id]).try :with_geometry
      end

      #
      # If the request contains a +state_id+, fetches the State with that
      # +geoid+.
      #
      def state
        return @state if instance_variable_defined? :@state
        @state = Patchwerks::Local::State.for_geoid params[:state_id]
      end
  end
end
