#
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
module Patchwerks

  #
  # Contains a Rails application that exports Patchwerks Local as a REST API.
  #
  module Api

    #
    # Base class for all Patchwerks API controllers.
    #
    class ApplicationController < ActionController::Base
      around_filter :in_transaction
      protect_from_forgery

      protected

        #
        # Wraps each request in an ActiveRecord transaction, so that no request
        # that results in an error has side effects.
        #
        def in_transaction(&block)
          ActiveRecord::Base.transaction &block
        end

        #
        # If a record is available, renders with default options. If a record is
        # not available, enqueues the record and renders HTTP 503 Service
        # Unavailable. If a record is not found, renders HTTP 404 Not Found.
        #
        # [record] The Patchwerks object to use for determining the response.
        #
        def respond_with(record)
          if record.nil?
            render 'not_found', status: :not_found
          elsif !record.available?
            record.enqueue
            render 'service_unavailable', status: :service_unavailable
          end
        end
    end
  end
end
