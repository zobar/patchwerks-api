#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

module Patchwerks::Api

  #
  # Controller that manages CensusTracts.
  #
  class CensusTractsController < ApplicationController
    helper 'patchwerks/api/svg'
    helper_method :census_tract, :census_tracts, :county, :state

    #
    # GET /states/36/census_tracts.svg
    # GET /counties/36029/census_tracts.svg
    #
    def index
      respond_to do |format|
        format.svg {respond_with census_tracts}
      end
    end

    #
    # GET /census_tracts/36029005000.svg
    #
    def show
      respond_to do |format|
        format.svg {respond_with census_tract}
      end
    end

    protected

      #
      # Fetches the requested CensusTract.
      #
      def census_tract
        return @census_tract if instance_variable_defined? :@census_tract
        @census_tract = Patchwerks::Local::CensusTract.for_geoid params[:id]
      end

      #
      # If the request contains a +county_id+, fetches all CensusTracts for the
      # County with that +geoid+. If the request contains a +state_id+, fetches
      # all CensusTracts for the State with that +geoid+.
      #
      def census_tracts
        return @census_tracts if instance_variable_defined? :@census_tracts
        @census_tracts = if params.has_key? :county_id
          Patchwerks::Local::CensusTract.for_parent_geoid(
              params[:county_id]).try :with_geometry
        elsif params.has_key? :state_id
          Patchwerks::Local::CensusTract.for_parent_geoid(
              params[:state_id]).try :with_geometry
        end
      end

      #
      # If the request contains a +county_id+, fetches the County with that
      # +geoid+.
      #
      def county
        return @county if instance_variable_defined? :@county
        @county = Patchwerks::Local::County.for_geoid params[:county_id]
      end

      #
      # If the request contains a +state_id+, fetches the State with that
      # +geoid+.
      #
      def state
        return @state if instance_variable_defined? :@state
        @state = Patchwerks::Local::State.for_geoid params[:state_id]
      end
  end
end
