xml.instruct! :xml, version: '1.0'
xml.svg viewBox: svg_view_box(census_tract), version: '1.1',
        xmlns: 'http://www.w3.org/2000/svg' do |svg|
  svg.title census_tract.name
  svg.g transform: svg_transform(census_tract) do |g|
    g.path d: svg_path_data(census_tract), id: census_tract.geoid
  end
end
