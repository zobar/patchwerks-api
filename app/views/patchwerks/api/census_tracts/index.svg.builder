xml.instruct! :xml, version: '1.0'
xml.svg :viewBox       => svg_view_box(*census_tracts),
        :version       => '1.1',
        :xmlns         => 'http://www.w3.org/2000/svg',
        :'xmlns:xlink' => 'http://www.w3.org/1999/xlink' do |svg|
  svg.title(
      county.try(:name) || state.try(:name) ||
      Patchwerks::Local::Tiger::CensusTract.model_name.human(
      count: census_tracts.count))
  svg.g transform: svg_transform(*census_tracts) do |g|
    census_tracts.each do |census_tract|
      g.a :'xlink:href' => census_tract_url(census_tract, format: :svg) do |a|
        a.title census_tract.name
        a.path d: svg_path_data(census_tract), id: census_tract.geoid
      end
    end
  end
end
