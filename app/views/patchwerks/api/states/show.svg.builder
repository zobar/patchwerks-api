xml.instruct! :xml, version: '1.0'
xml.svg viewBox: svg_view_box(state), version: '1.1',
        xmlns: 'http://www.w3.org/2000/svg' do |svg|
  svg.title state.name
  svg.g transform: svg_transform(state) do |g|
    g.path d: svg_path_data(state), id: state.geoid
  end
end
