xml.instruct! :xml, version: '1.0'
xml.svg :viewBox       => svg_view_box(*states),
        :version       => '1.1',
        :xmlns         => 'http://www.w3.org/2000/svg',
        :'xmlns:xlink' => 'http://www.w3.org/1999/xlink' do |svg|
  svg.title(
      Patchwerks::Local::Tiger::State.model_name.human(count: states.count))
  svg.g transform: svg_transform(*states) do |g|
    states.each do |state|
      g.a :'xlink:href' => state_url(state, format: :svg) do |a|
        a.title state.name
        a.path d: svg_path_data(state), id: state.geoid
      end
    end
  end
end
