xml.instruct! :xml, version: '1.0'
xml.svg :version       => '1.1',
        :xmlns         => 'http://www.w3.org/2000/svg' do |svg|
  svg.title t('http.service_unavailable.title')
end
