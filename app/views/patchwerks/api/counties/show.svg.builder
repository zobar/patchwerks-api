xml.instruct! :xml, version: '1.0'
xml.svg viewBox: svg_view_box(county), version: '1.1',
        xmlns: 'http://www.w3.org/2000/svg' do |svg|
  svg.title county.name
  svg.g transform: svg_transform(county) do |g|
    g.path d: svg_path_data(county), id: county.geoid
  end
end
