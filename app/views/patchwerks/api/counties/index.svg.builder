xml.instruct! :xml, version: '1.0'
xml.svg :viewBox       => svg_view_box(*counties),
        :version       => '1.1',
        :xmlns         => 'http://www.w3.org/2000/svg',
        :'xmlns:xlink' => 'http://www.w3.org/1999/xlink' do |svg|
  svg.title(
      state.try(:name) ||
      Patchwerks::Local::Tiger::County.model_name.human(count: counties.count))
  svg.g transform: svg_transform(*counties) do |g|
    counties.each do |county|
      g.a :'xlink:href' => county_url(county, format: :svg) do |a|
        a.title county.name
        a.path d: svg_path_data(county), id: county.geoid
      end
    end
  end
end
