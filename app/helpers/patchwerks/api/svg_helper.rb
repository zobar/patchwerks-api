#
# Helper functions related to rendering Patchwerks objects into SVG.
#
module Patchwerks::Api::SvgHelper

  #
  # +svg_path_data+ provides some rudimentary compression by scaling all
  # coordinates so that there are no decimals. This constant determines what
  # factor to scale by.
  #
  SCALE = 1000000

  #
  # Used by the SVG helpers to determine the bounding box of geometry objects.
  # Returns an RGeo::Cartesian::BoundingBox.
  #
  # [model]  Object to retrieve the bounding box for. This is expected to be a
  #          Patchwerks model.
  # [models] Additional models to add to the bounding box.
  #
  def svg_bounding_box(model, *models)
    return @svg_bounding_box if instance_variable_defined? :@svg_bounding_box
    @svg_bounding_box = RGeo::Cartesian::BoundingBox.create_from_geometry(
        model.geometry)
    models.each{|m| @svg_bounding_box.add m.geometry}
    @svg_bounding_box
  end

  #
  # Return an SVG path data string for a given geometry object. This performs
  # some rudimentary compression by using relative +lineto+ operations rather
  # than absolute. (<tt>L-78.850461 42.959568</tt> uses a couple more bytes than
  # <tt>l-0.000002 0.00001</tt>.) Further compression is achieved by scaling the
  # coordinates so that there are no decimals. (Thus,
  # <tt>l-0.000002 0.00001</tt> becomes <tt>l-2 10</tt>.)
  #
  # [geometry] An object to return path data for. This can be an RGeo::Feature,
  #            any object that implements a +geometry+ method, or an Enumerable
  #            of either.
  # [io]       An object that will be used to collect the path data. Must
  #            respond to the +<<+ operator.
  #
  def svg_path_data(geometry, io='')
    if geometry.is_a? Enumerable
      geometry.each {|i| svg_path_data(i, io)}
    elsif geometry.respond_to? :geometry
      svg_path_data geometry.geometry, io
    else
      case geometry
      when RGeo::Feature::LinearRing
        point = geometry.point_n 0
        io << format('M%d% d', point.x * SCALE, point.y * SCALE)
        geometry.points.each_cons 2 do |previous, point|
          dx = point.x - previous.x
          dy = point.y - previous.y
          if dx == 0
            io << format('v%d', dy * SCALE)
          elsif dy == 0
            io << format('h%d', dx * SCALE)
          else
            io << format('l%d% d', dx * SCALE, dy * SCALE)
          end
        end
        io << 'Z'
      when RGeo::Feature::Polygon
        svg_path_data geometry.exterior_ring, io
        svg_path_data geometry.interior_rings, io
      end
    end
    io
  end

  #
  # The equirectangular projection used by the SVG helpers introduces a fair
  # amount of east-west distortion at greater latitudes. To minimize this
  # effect, the entire image is squashed horizontally by the cosine of the
  # latitude at the vertical center of the map.
  #
  # [geometries] The geometries to use for calculating scaling.
  #
  def svg_scale_x(*geometries)
    return @svg_scale_x if instance_variable_defined? :@svg_scale_x
    bbox = svg_bounding_box *geometries
    @svg_scale_x = Math.cos(bbox.center_y * Math::PI / 180)
  end

  #
  # Returns an SVG transformation matrix that should be applied to the
  # geometries in this image.
  #
  # [geometries] The geometries to use for calculating the transform.
  #
  def svg_transform(*geometries)
    bbox = svg_bounding_box *geometries
    scale_x = svg_scale_x *geometries
    format 'matrix(%.6f 0 0 -1 %d %d)', scale_x, -bbox.min_x * scale_x * SCALE,
           bbox.max_y * SCALE
  end

  #
  # Returns an SVG view box that contains all the geometries given.
  #
  # [geometries] The geometries to use for calculating the view box.
  #
  def svg_view_box(*geometries)
    bbox = svg_bounding_box *geometries
    scale_x = svg_scale_x *geometries
    format '0 0 %d %d', (bbox.max_x - bbox.min_x) * scale_x * SCALE,
           (bbox.max_y - bbox.min_y) * SCALE
  end
end
