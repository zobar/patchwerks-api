#
# Application-wide helper functions. It is preferable to group related helper
# functions in their own module and include them in your controller like so:
#
#   helper :foo
#
module Patchwerks::Api::ApplicationHelper
end
