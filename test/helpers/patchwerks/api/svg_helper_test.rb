require 'test_helper'

describe Patchwerks::Api::SvgHelper do
  let :bbox do
    RGeo::Cartesian::BoundingBox.create_from_geometry state.geometry
  end

  let :state do
    Patchwerks::Local::State.for_geoid '00'
  end

  subject do
    Object.new.tap do |subject|
      class << subject
        include Patchwerks::Api::SvgHelper
      end
    end
  end

  it 'scales horizontally based on latitude' do
    Patchwerks::Local::State.mock do
      subject.svg_scale_x(state).must_be_within_epsilon 0.879486, 0.000001
    end
  end

  it 'converts geometry to SVG path data' do
    Patchwerks::Local::State.mock do
      subject.svg_path_data(state).must_equal(
          'M-139474624 27919623h-1000000v1000000l500000-500000l500000-500000Z')
    end
  end

  it 'calculates a projection transform' do
    Patchwerks::Local::State.mock do
      subject.svg_transform(state).must_equal(
          'matrix(0.879486 0 0 -1 123545412 28919623)')
    end
  end

  it 'calculates a view box' do
    Patchwerks::Local::State.mock do
      subject.svg_view_box(state).must_equal '0 0 879485 1000000'
    end
  end

  describe 'svg_bounding_box' do
    let :state_2 do
      Patchwerks::Local::State.for_geoid('01').tap do |state|
        factory = state.geometry.factory
        state.geometry = factory.multi_polygon([
          factory.polygon(
            factory.linear_ring([
              factory.point(-138.474624, 28.919623),
              factory.point(-139.474624, 28.919623),
              factory.point(-139.474624, 29.919623),
              factory.point(-138.474624, 29.919623)]))])
      end
    end

    it 'must handle multiple geometries' do
      Patchwerks::Local::State.mock do
        bbox.add state_2.geometry
        bbox_2 = subject.svg_bounding_box state, state_2
        bbox.must_equal bbox_2
      end
    end

    it 'must handle one geometry' do
      Patchwerks::Local::State.mock do
        bbox_2 = subject.svg_bounding_box state
        bbox.must_equal bbox_2
      end
    end
  end

end
