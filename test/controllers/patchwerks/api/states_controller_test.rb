require 'test_helper'

describe Patchwerks::Api::StatesController do
  it 'must return OK from index when loaded' do
    Patchwerks::Local::State.mock do
      get :index, format: :svg
      assert_response :ok
    end
  end

  it 'must return Service Unavailable from index when not loaded' do
    Patchwerks::Local::State.mock :pending do
      get :index, format: :svg
      assert_response :service_unavailable
    end
  end

  it 'must return Not Found from show when loaded and not found' do
    Patchwerks::Local::State.mock :not_found do
      get :show, format: :svg, id: '00'
      assert_response :not_found
    end
  end

  it 'must return OK from show when loaded' do
    Patchwerks::Local::State.mock do
      get :index, format: :svg
      assert_response :ok
    end
  end

  it 'must return Service Unavailable from show when not loaded' do
    Patchwerks::Local::State.mock :pending do
      get :show, format: :svg, id: '00'
      assert_response :service_unavailable
    end
  end
end
