require 'test_helper'

describe Patchwerks::Api::CensusTractsController do
  it 'must return OK from state index when loaded' do
    Patchwerks::Local::State.mock do
      Patchwerks::Local::CensusTract.mock do
        get :index, format: :svg, state_id: '00'
        assert_response :ok
      end
    end
  end

  it 'must return OK from county index when loaded' do
    Patchwerks::Local::County.mock do
      Patchwerks::Local::CensusTract.mock do
        get :index, format: :svg, county_id: '00000'
        assert_response :ok
      end
    end
  end

  it 'must return Service Unavailable from state index when not loaded' do
    Patchwerks::Local::CensusTract.mock :pending do
      get :index, format: :svg, state_id: '00'
      assert_response :service_unavailable
    end
  end

  it 'must return Service Unavailable from county index when not loaded' do
    Patchwerks::Local::CensusTract.mock :pending do
      get :index, format: :svg, county_id: '00000'
      assert_response :service_unavailable
    end
  end

  it 'must return Not Found from state index when loaded and not found' do
    Patchwerks::Local::CensusTract.mock :not_found do
      get :index, format: :svg, state_id: '00'
      assert_response :not_found
    end
  end

  it 'must return Not Found from county index when loaded and not found' do
    Patchwerks::Local::CensusTract.mock :not_found do
      get :index, format: :svg, county_id: '00000'
      assert_response :not_found
    end
  end

  it 'must return OK from show when loaded' do
    Patchwerks::Local::CensusTract.mock do
      get :show, format: :svg, id: '00000000000'
      assert_response :ok
    end
  end

  it 'must return Service Unavailable from show when not loaded' do
    Patchwerks::Local::CensusTract.mock :pending do
      get :show, format: :svg, id: '00000000000'
      assert_response :service_unavailable
    end
  end

  it 'must return Not Found from show when loaded and not found' do
    Patchwerks::Local::CensusTract.mock :not_found do
      get :show, format: :svg, id: '00000000000'
      assert_response :not_found
    end
  end
end
