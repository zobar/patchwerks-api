require 'test_helper'

describe Patchwerks::Api::CountiesController do
  it 'must return OK from index when loaded' do
    Patchwerks::Local::County.mock do
      get :index, format: :svg
      assert_response :ok
    end
  end

  it 'must return OK from state index when loaded' do
    Patchwerks::Local::State.mock do
      Patchwerks::Local::County.mock do
        get :index, format: :svg, state_id: '00'
        assert_response :ok
      end
    end
  end

  it 'must return Service Unavailable from index when not loaded' do
    Patchwerks::Local::County.mock :pending do
      get :index, format: :svg
      assert_response :service_unavailable
    end
  end

  it 'must return Service Unavailable from state index when not loaded' do
    Patchwerks::Local::County.mock :pending do
      get :index, format: :svg, state_id: '00'
      assert_response :service_unavailable
    end
  end

  it 'must return Not Found from state index when loaded and not found' do
    Patchwerks::Local::County.mock :not_found do
      get :index, format: :svg, state_id: '00'
      assert_response :not_found
    end
  end

  it 'must return OK from show when loaded' do
    Patchwerks::Local::County.mock do
      get :show, format: :svg, id: '00000'
      assert_response :ok
    end
  end

  it 'must return Service Unavailable from show when not loaded' do
    Patchwerks::Local::County.mock :pending do
      get :show, format: :svg, id: '00000'
      assert_response :service_unavailable
    end
  end

  it 'must return Not Found from show when loaded and not found' do
    Patchwerks::Local::County.mock :not_found do
      get :show, format: :svg, id: '00000'
      assert_response :not_found
    end
  end
end
