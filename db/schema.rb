# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 2) do

  create_table "patchwerks_local_shapes", :force => true do |t|
    t.integer  "owner_id",                                                    :null => false
    t.string   "owner_type",                                                  :null => false
    t.datetime "created_at",                                                  :null => false
    t.datetime "updated_at",                                                  :null => false
    t.spatial  "geometry",   :limit => {:srid=>4326, :type=>"multi_polygon"}
    t.spatial  "intpt",      :limit => {:srid=>4326, :type=>"point"}
  end

  add_index "patchwerks_local_shapes", ["geometry"], :name => "index_patchwerks_local_shapes_on_geometry", :spatial => true
  add_index "patchwerks_local_shapes", ["owner_type", "owner_id"], :name => "index_patchwerks_local_shapes_on_owner_type_and_owner_id", :unique => true

  create_table "patchwerks_local_tiger_census_tracts", :force => true do |t|
    t.string   "statefp",    :limit => 2,  :null => false
    t.string   "countyfp",   :limit => 3,  :null => false
    t.string   "tractce",    :limit => 6,  :null => false
    t.string   "geoid",      :limit => 11, :null => false
    t.string   "name",       :limit => 7,  :null => false
    t.string   "namelsad",   :limit => 20, :null => false
    t.integer  "aland",      :limit => 8,  :null => false
    t.integer  "awater",     :limit => 8,  :null => false
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "patchwerks_local_tiger_census_tracts", ["geoid"], :name => "index_patchwerks_local_tiger_census_tracts_on_geoid", :unique => true
  add_index "patchwerks_local_tiger_census_tracts", ["statefp", "countyfp"], :name => "index_patchwerks_local_tiger_census_tracts_on_statefp_and_count"

  create_table "patchwerks_local_tiger_counties", :force => true do |t|
    t.string   "statefp",    :limit => 2,   :null => false
    t.string   "countyfp",   :limit => 3,   :null => false
    t.string   "countyns",   :limit => 8,   :null => false
    t.string   "geoid",      :limit => 5,   :null => false
    t.string   "name",       :limit => 100, :null => false
    t.string   "namelsad",   :limit => 100, :null => false
    t.string   "lsad",       :limit => 2,   :null => false
    t.string   "classfp",    :limit => 2,   :null => false
    t.string   "csafp",      :limit => 3
    t.string   "cbsafp",     :limit => 5
    t.string   "metdivfp",   :limit => 5
    t.string   "funcstat",   :limit => 1,   :null => false
    t.integer  "aland",      :limit => 8,   :null => false
    t.integer  "awater",     :limit => 8,   :null => false
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "patchwerks_local_tiger_counties", ["geoid"], :name => "index_patchwerks_local_tiger_counties_on_geoid", :unique => true
  add_index "patchwerks_local_tiger_counties", ["statefp"], :name => "index_patchwerks_local_tiger_counties_on_statefp"

  create_table "patchwerks_local_tiger_files", :force => true do |t|
    t.boolean  "loaded",        :null => false
    t.string   "class_name",    :null => false
    t.string   "upstream_file", :null => false
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "patchwerks_local_tiger_files", ["class_name"], :name => "index_patchwerks_local_tiger_files_on_class_name"
  add_index "patchwerks_local_tiger_files", ["upstream_file"], :name => "index_patchwerks_local_tiger_files_on_upstream_file", :unique => true

  create_table "patchwerks_local_tiger_states", :force => true do |t|
    t.string   "region",     :limit => 2,   :null => false
    t.string   "division",   :limit => 2,   :null => false
    t.string   "statens",    :limit => 8,   :null => false
    t.string   "geoid",      :limit => 2,   :null => false
    t.string   "stusps",     :limit => 2,   :null => false
    t.string   "name",       :limit => 100, :null => false
    t.integer  "aland",      :limit => 8,   :null => false
    t.integer  "awater",     :limit => 8,   :null => false
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "patchwerks_local_tiger_states", ["geoid"], :name => "index_patchwerks_local_tiger_states_on_geoid", :unique => true

end
